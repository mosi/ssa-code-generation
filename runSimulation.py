import argparse
import toml
import subprocess
import os
from timeit import default_timer as timer
import random
import pathlib
import jinja2

from prettytable import PrettyTable

def parse_bngl_network(filename):
    f = open(filename)

    def constant_replace(consts, s):
        s = ' ' + s + ' '
        for v in ('*', '/', '(', ')', '+', '-'):
            s = s.replace(v, ' ' + v + ' ')
        for v in consts.keys():
            if v in s:
                s = s.replace(' ' + v + ' ', ' ' + consts[v] + ' ')
        return s.replace(' ', '')

    f = f.read()

    f = f.split('\n')

    # remove comments
    f2 = []
    for l in f:
        l.lstrip(' ')
        l = l.split('#')
        if l[0] != '':
            f2.append(l[0].lstrip(' '))

    f = f2

    assert (f[0] == 'begin parameters')
    f.pop(0)
    constants = {}
    num = 1
    while f[0].split(' ')[0] == str(num):
        l = f[0].split(' ')
        while '' in l: l.remove('')
        constants.update({l[1]: l[2]})

        num += 1
        f.pop(0)
    assert (f[0] == 'end parameters')
    f.pop(0)

    assert (f[0] == 'begin species')
    f.pop(0)

    init_species = []
    num = 1
    while f[0].split(' ')[0] == str(num):
        l = f[0].split(' ')
        while '' in l: l.remove('')
        init_species.append(int(float(constant_replace(constants, l[2]))))

        num += 1
        f.pop(0)
    assert (f[0] == 'end species')
    f.pop(0)

    assert (f[0] == 'begin reactions')
    f.pop(0)

    rules = []
    num = 1
    reac_add_counter = 0
    while f[0].split(' ')[0] == str(num):
        l = f[0].split(' ')
        while '' in l: l.remove('')

        species_in = list(map(lambda y: int(y) - 1, filter(lambda x: x != '0', l[1].split(','))))
        species_out = list(map(lambda y: int(y) - 1, filter(lambda x: x != '0', l[2].split(','))))
        rate = constant_replace(constants, l[3])
        rate = float(eval(constant_replace(constants, rate)))
        rule = {'in': species_in, 'out': species_out, 'rate': rate, 'num': reac_add_counter}

        if rate != 0 or False:
            rules.append(rule)
            reac_add_counter += 1

        num += 1
        f.pop(0)
    assert (f[0] == 'end reactions')
    f.pop(0)

    assert (f[0] == 'begin groups')
    f.pop(0)

    observations = {}
    num = 1
    while f[0].split(' ')[0] == str(num):
        l = f[0].split(' ')
        while '' in l: l.remove('')
        len(l) == 2
        l.append('0')
        species_obs = list(filter(lambda x: x != '0', l[2].split(',')))

        new_spec_obs = []
        for i in species_obs:
            if '*' in i:
                # print(i.split('*'))
                for _ in range(int(i.split('*')[0])):
                    new_spec_obs.append(int(i.split('*')[1]) - 1)
            else:
                new_spec_obs.append(int(i) - 1)

        observations.update({l[1]: new_spec_obs})

        num += 1
        f.pop(0)
    assert (f[0] == 'end groups')
    f.pop(0)
    assert (len(f) == 0)
    # print(constants)

    model = {'init': init_species, 'rules': rules, 'observations': observations}
    return model

def parse_custom_reaction_lang(filename):
    metamod = textx.metamodel_from_file('python_src/grammar.tx')
    with open(filename, 'r') as file:
        model_str = file.read()
        model = metamod.model_from_str(model_str)

    print("# - PARISING COMPLETE - #")

    assert(model.until > model.delta_t)
    assert(model.delta_t > 0)
    model.num_steps = int(model.until/model.delta_t)


    if len(model.observations) == 0:
        model.observations = model.species
    num = 0
    for ent in model.observations:
        ent.observation_num = num
        num = num + 1
    model.num_observation = num


    # shuffle all elements of model
    if True:
        random.shuffle(model.species)
        #model.species=sorted(model.species, key=lambda e: e.name)
        random.shuffle(model.rules)
        for r in model.rules:
            random.shuffle(r.inp)
            random.shuffle(r.out)

        # give each species a number
    num = 0
    for ent in model.species:
            ent.in_rate_calc = []
            ent.num = num
            num = num + 1
            ent.inital_amount = 0

            ent.amount_modified_in = set()

    model.num_species = num

    # find initial_amount from INIT
    for init in model.inital:
            assert (init.species.inital_amount == 0)
            init.species.inital_amount = init.init_amt

    return generate_toml_dic(model)



def generate_toml_dic(model):
    res = dict()
    #res["seeds"] = model.seeds
    res["init"] = list(map(lambda e: e.inital_amount,model.species))
    #res["num_rules"] = model.num_rules
    res["num_steps"] = model.num_steps
    #res["timestep"] = model.delta_t
    res["until"] = float(model.until)

    res["observations"] = {}

    for i in model.observations:
        res["observations"]["num_"+i.name] =  [i.num,]

    rules = []
    for (count,r) in enumerate(model.rules):
        my_dict = dict()
        my_dict["num"] = count #r.num
        my_dict["in"] = list(map(lambda e: e.num,r.inp))
        my_dict["out"] = list(map(lambda e: e.num,r.out))

        assert(not r.comparator)
        #print(' COMPO : '+ str(r.comparator))
        #assert (not hasattr(r,'comparator')) # currently not supported

        is_state = collections.Counter(my_dict["in"])
        should_state = list(map(lambda e: e.ent.num,filter(lambda mr: hasattr(mr,'ent') and hasattr(mr.ent,'num'), r.rate.elem)))
        #print(is_state)
        #print(list(map(lambda e: e.ent.name,filter(lambda mr: hasattr(mr,'ent') and hasattr(mr.ent,'num'), r.rate.elem))))
        assert(is_state == collections.Counter(should_state) )

        rate_num = 1.0
        for i in list(map(lambda e: e.num, filter(lambda mr: hasattr(mr, 'num') and mr.num >0, r.rate.elem))):
            rate_num *= i
        my_dict["rate"] = rate_num
        r.aggregated_rate_constant = rate_num
        rules.append(my_dict)
        #res["rule_" + str(r.num)] = my_dict
    res["rules"] = rules
    return res

def optimize_model_order(mod, opt_file):
    counts = toml.load(opt_file)
    counts = counts["reac_counts"]
    # print(str(len(mod['rules']))+ ' == '+ str( len(counts)))
    assert (len(mod['rules']) == len(counts))
    assert (not "affected_reaction" in mod)
    mod['rules'] = list(map(lambda v: v[1], sorted(enumerate(mod['rules']), key=lambda x: counts[x[0]], reverse=True)))
    for i in range(len(mod['rules'])):
        mod['rules'][i]['num'] = i
        # print(str(i)+ ' =#= ' + str(r['num']))
        # assert(i==int(r['num']))
    return mod


def model2toml(filename):
    file_ext = filename.split('.')[-1]
    if file_ext == "net":
        return parse_bngl_network(filename)
    elif file_ext == "reaction_model":
        return parse_custom_reaction_lang(filename)
    else:
        raise Exception("Unnokwn extension " + filename + " -> "+file_ext)





def generate_cpp_code(model, ccompiler, compile=True):
    assert (isinstance(model["until"], float))
    assert (model["num_steps"] > 1)

    with open('templates/basic_code_generation.cpp') as file_:
        template = jinja2.Template(file_.read())

    with open('generated_code/src/main.cpp', 'w') as f:
        f.write(template.render(model=model))

    os.system("clang-format -i generated_code/src/*.cpp")

    # print(model['rules'][0])
    if compile:
        assert (os.system(ccompiler + ' -O3 -march=native -LTO generated_code/src/main.cpp -o prog_c.bin') == 0)




def generate_rust_code(model, compile=True):
    assert (isinstance(model["until"], float))
    assert (model["num_steps"] > 1)

    thedir = "generated_code/src"
    if not os.path.exists(thedir):
        os.makedirs(thedir)

    with open('templates/main_code_template.rs') as file_:
        template = jinja2.Template(file_.read())

    with open('templates/rule_module_template.rs') as file_:
        template_mod = jinja2.Template(file_.read())

    with open('generated_code/src/bin.rs', 'w') as f:
        f.write(template.render(model=model))

    with open('generated_code/src/reactions_mod_master.rs', 'w') as f:
        f.write(template_mod.render(model=model))

    with open('templates/lib_struct_template.rs') as file_:
        template_struct = jinja2.Template(file_.read())
    with open('generated_code/src/lib.rs', 'w') as f:
        f.write(template_struct.render(model=model))
    print("Formatting Rust Code")
    subprocess.check_output("rustfmt generated_code/src/*.rs", shell=True)

    # print(model['rules'][0])
    if compile:
        start_compile = timer()
        print("# Compiling Rust Code")
        subprocess.check_output("cargo build -j 1 --manifest-path=generated_code/Cargo.toml --release", shell=True,
                                stderr=subprocess.STDOUT)
        compile_time = timer() - start_compile
        return compile_time



def fix_numbers(mod):
    mod2 = mod.copy()
    new_affe = {}
    for k in mod2["affected_reaction"].keys():
        new_affe[str(k)] = mod["affected_reaction"][k]
    mod2["affected_reaction"] = new_affe

    return mod2

def preprocess_model(model):
    # optimize rules
    for r in model['rules']:
        new_in = r['in'].copy()
        new_out = []
        for o in r['out']:
            if o in new_in:
                new_in.remove(o)
            else:
                new_out.append(o)

        r['opt_in'] = new_in
        r['opt_out'] = new_out

        r['min_of'] = {}
        for i in r['opt_in']:
            if r['opt_in'].count(i) > 1:
                r['min_of'][str(i)] = r['opt_in'].count(i)
        # assert(len(new_in) == len(set(new_in))) # can not correctly handle double remove

    # find affected rules graph
    species_changed_in_rule = {}
    for r in model['rules']:
        for s in set(r['opt_in']).union(set(r['opt_out'])):
            if not s in species_changed_in_rule:
                species_changed_in_rule[s] = {r['num']}
            else:
                species_changed_in_rule[s].add(r['num'])

    # find affected rules graph
    rule_affects_rule = {}
    for r in model['rules']:
        rule_affects_rule[r['num']] = set()
        for s in set(r['in']):
            for other_r in species_changed_in_rule[s]:
                rule_affects_rule[r['num']].add(other_r)

    for r in model['rules']:
        rule_affects_rule[r['num']] = list(rule_affects_rule[r['num']])

    model["affected_reaction"] = rule_affects_rule



def shuffle_model(model):
    new_reac_nums = list(range(len(model["rules"])))
    random.shuffle(new_reac_nums)

    for r, n in zip(model["rules"], new_reac_nums):
        r["num"] = n

    new_pop_nums = list(range(len(model["init"])))
    random.shuffle(new_pop_nums)
    # print(new_pop_nums)
    # print(model["init"])
    model["init"] = list(map(lambda a: a[0], sorted(zip(model["init"], new_pop_nums), key=lambda x: x[1])))
    # print(model["init"])

    for r in model["rules"]:
        r["in"] = list(map(lambda n: new_pop_nums[n], r["in"]))
        r["out"] = list(map(lambda n: new_pop_nums[n], r["out"]))
    model["rules"].sort(key=lambda r: r["num"])
    return model


print_output = True


def get_bionetgen_path():
    bnglpath = 'BioNetGen-2.5.0/bin/run_network'

    if (not os.path.isfile(bnglpath)):
        print("# could not find bionetgen. Try autodownloaiding")
        os.system(
            'wget https://github.com/RuleWorld/bionetgen/releases/download/BioNetGen-2.5.0/BioNetGen-2.5.0-linux.tgz')
        os.system('tar xvzf  BioNetGen-2.5.0-linux.tgz')
    if (not os.path.isfile(bnglpath)):
        raise Exception(bnglpath + " does not exist")

    return bnglpath


def core_execution_function(modelfile,optimize,steps,until,target,only_generate=False):
    if( not os.path.isfile(modelfile)):
        raise Exception("File does not exist: "+modelfile)

    extention = pathlib.Path(modelfile).suffix

    if (target == "bionetgen" and optimize):
        raise Exception("Bionetgen is not compatible with optimization option")


    assert(steps > 1)
    assert(until > 0)
    if(not target in ["rust","gcc","generic","clang","bionetgen"]):
        raise Exception("Unsuported target: "+target)

    if (not extention in [".toml",".net",".reaction_model"]):
        raise Exception("Unknown file extention "+extention)

    start_parse = timer()

    if(extention == ".toml"):
        m = toml.load(open(modelfile))
    else:
        m = model2toml(modelfile)


    m = shuffle_model(m)
    unprocesd_model = m.copy()

    preprocess_model(m)
    stop_parse = timer()

    m["num_steps"] = int(steps)
    m["until"] = float(until)






    def check_rust():
        try:
            subprocess.check_output('cargo -V', shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError:
            raise Exception("Rust does not work. 'cargo -V' failed")

    start_sort = timer()
    if(optimize):
        print("# Preparing counting simulator")
        check_rust()
        subprocess.check_output('cargo build --manifest-path=generic_simulator/Cargo.toml --release', shell=True, stderr=subprocess.STDOUT)

        if os.path.exists("reaction_counts.toml"):
            os.remove("reaction_counts.toml")
        #generate_parsed(m)
        toml.dump(fix_numbers(m), open("temporary_model.toml", "w"))

        print("# Running counting simulator")
        subprocess.check_output('cargo run --manifest-path=generic_simulator/Cargo.toml --release -- temporary_model.toml --count', shell=True,
                                stderr=subprocess.STDOUT)
        assert(os.path.exists("reaction_counts.toml"))
        m = optimize_model_order(unprocesd_model, "reaction_counts.toml")
        preprocess_model(m)
        m["num_steps"] = int(steps)
        m["until"] = float(until)
        stop_sort = timer()
    else:
        stop_sort = start_sort



    print("# running with: "+target)

    start_compile = timer()

    if target == "rust":
        check_rust()
        generate_rust_code(m)
        stop_compile = timer()
        start_run = timer()
        print("Running generated rust code")
        if (not only_generate):
            out = subprocess.check_output("cargo run --manifest-path=generated_code/Cargo.toml --release", shell=True,stderr=subprocess.STDOUT)
        else:
            return
        #out = subprocess.check_output("generated_code/target/release/run_model", shell=True, stderr=subprocess.STDOUT)
        stop_run = timer();
        prop_time = out.decode("utf-8").split("Steps in ")[1].split(" -> ")[0]
        reported_time = float(prop_time)
        reported_steps = int(out.decode("utf-8").split("# --- ")[1].split("Steps in")[0])
        if print_output:
            print(out.decode("utf-8"))

    elif target == "generic":
            check_rust()
            toml.dump(fix_numbers(m), open("temporary_model.toml", "w"))
            stop_compile = start_compile
            subprocess.check_output('cargo build --manifest-path=generic_simulator/Cargo.toml --release', shell=True,
                                    stderr=subprocess.STDOUT)

            start_run = timer()
            # out = subprocess.check_output('cargo run --manifest-path=parsed_runner/Cargo.toml --release', shell=True,stderr=subprocess.STDOUT)
            if (not only_generate):
                out = subprocess.check_output('generic_simulator/target/release/generic_simulator temporary_model.toml', shell=True,
                                          stderr=subprocess.STDOUT)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
            reported_steps = int(out.decode("utf-8").split("# --- ")[1].split("Steps in")[0])
            reported_time = float(prop_time)
            if print_output:
                print(out)

    elif target == "gcc":
            try:
                subprocess.check_output('g++ -v', shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                raise Exception("gcc does not work. 'g++ -v' failed")
            if os.path.exists("prog_c.bin"):
                os.remove("prog_c.bin")
            generate_cpp_code(m, "g++")
            stop_compile = timer()
            start_run = timer()
            if (not only_generate):
                out = subprocess.check_output('./prog_c.bin', shell=True)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
            reported_time = float(prop_time)
            reported_steps = int(out.decode("utf-8").split("# -C- ")[1].split("Steps in")[0])
            if print_output:
                print(out)

    elif target == "clang":
            try:
                subprocess.check_output('g++ -v', shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                raise Exception("gcc does not work. 'g++ -v' failed")
            if os.path.exists("prog_c.bin"):
                os.remove("prog_c.bin")
            generate_cpp_code(m, "clang++")
            stop_compile = timer()
            start_run = timer()
            if (not only_generate):
                out = subprocess.check_output('./prog_c.bin', shell=True)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
            reported_time = float(prop_time)
            reported_steps = int(out.decode("utf-8").split("# -C- ")[1].split("Steps in")[0])
            if print_output:
                print(out)
    elif target == "bionetgen":
            if (extention != '.net'):
                raise Exception("Bionetgen target only supports .net files")
            stop_compile = start_compile
            bnglpath = get_bionetgen_path()


            start_run = timer()
            bng_cmd = bnglpath + " -o ./bngl_output -p ssa -h " + str(random.randint(10, 1000000000)) + " --cdat 1 --fdat 0 -g ./" + modelfile + " " + " ./" + modelfile + " " + str(
                m["until"] / m["num_steps"]) + " " + str(m["num_steps"])
            print(bng_cmd)
            if (not only_generate):
                out = subprocess.check_output(bng_cmd, shell=True)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Propagation took")[1].split("CPU seconds")[0]
            reported_time = float(prop_time)
            reported_steps = int(out.decode("utf-8").split("TOTAL STEPS: ")[1].split("Time course of")[0])
            if print_output:
                print(str(out.decode("utf-8")))
    else:
            raise Exception("Unsupported target " + str(target))



    return {"t_parsing": stop_parse - start_parse, "t_sort": stop_sort - start_sort,
            "t_compile": stop_compile - start_compile, "t_total_run": stop_run - start_run, "t_reported": reported_time,
            "num_steps": reported_steps}


if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser(description='Generate a specialized Simulator')
    parser.add_argument('model',help="the toml file of the model to use")
    parser.add_argument('--optimize', help="do a trial run first, to find optimal reaction order",action='store_true')
    parser.add_argument('--steps',type=int,help="The number of steps used for observation (default: 1000)",default=1000)
    parser.add_argument('--until',type=float,help="The time until to simulate (default: 10)",default=10)
    parser.add_argument("--target", help="name of the target(rust|generic|gcc|clang) (default: rust)",default="rust")
    parser.add_argument("--dry", help="only generate the needed files, but don't run the simulation", action='store_true')


    print_output = True



    args = parser.parse_args()

    res  = core_execution_function(args.model,args.optimize,args.steps,args.until,args.target,args.dry)

    print("STEPS: " + str(args.steps))

    x = PrettyTable()
    x.field_names = ["m","val [s]"]
    for val in res:
        x.add_row([val,res[val]])

    print(x)