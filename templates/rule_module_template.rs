
use crate::ModelStruct;


{% for rule in model["rules"] -%}
fn update_rule_{{-rule["num"]-}}(model : &mut ModelStruct){
        model.propensity_sum  -= model.propensity[{{rule["num"]}}];
        model.propensity[{{rule["num"]}}] = {{rule["rate"]}}
        {%- for elem in rule["in"] -%}
                * model.populations[{{elem}}] as f64
        {%- endfor -%}
        ;
        {% for min_s in rule['min_of'].keys() %}
        if model.populations[{{min_s}}] < {{rule['min_of'][min_s]}}{
            model.propensity[{{rule["num"]}}] = 0.0;
        }
        {% endfor -%}
        model.propensity_sum  += model.propensity[{{rule["num"]}}];
        }
{% endfor -%}

{% for r in model["rules"] -%}
pub fn execute_rule_{{-r["num"]-}}(model:&mut  ModelStruct){
    // Update state
    {%- for elem in r["opt_in"] %}
        model.populations[{{elem}}]-=1;
        {%- endfor -%}
         {%- for elem in r["opt_out"] %}
        model.populations[{{elem}}]+=1;
    {%- endfor %}

    // Update Propensities
    {% for affr in model["affected_reaction"][r["num"]] -%}
       update_rule_{{-affr-}}(model);
        {# model.propensity_sum  -= model.propensity[{{model["rules"][affr]["num"]}}];
        model.propensity[{{affr}}] = {{model["rules"][affr]["rate"]}}
        {%- for elem in model["rules"][affr]["in"] -%}
                * model.populations[{{elem}}] as f64
        {%- endfor -%}
        ;
        {% for min_s in model["rules"][affr]['min_of'].keys() %}
        if model.populations[{{min_s}}] < {{model["rules"][affr]['min_of'][min_s]}}{
            model.propensity[{{model["rules"][affr]["num"]}}] = 0.0;
        }
        {% endfor -%}
        model.propensity_sum  += model.propensity[{{affr}}];#}
    {%- endfor -%}
    }
{% endfor -%}