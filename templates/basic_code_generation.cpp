#include <cmath>
#include <random>
#include <iostream>
#include <chrono>
#include <iostream>
#include <fstream>
#include <stdexcept>



std::random_device rd;
std::mt19937_64 gen(rd());
// std::minstd_rand gen(rd());
std::uniform_real_distribution<> dis(0, 1.0);
std::exponential_distribution<> d_exp(1);
double randomDouble() { return dis(gen); }

double exp(double rate) { return d_exp(gen) / rate; }


size_t observations[{{model["num_steps"]}}+1][{{ model["observations"] | length }}];

double propensity[] = { {% for rule in model["rules"] -%} 0.0, {% endfor -%} };

size_t populations[] =  { {%- for e in model["init"] -%} {{e}}, {%- endfor -%} };

double    propensity_sum =0.0;
double    simtime = 0.0;

size_t  step_counter =0;


{% for rule in model["rules"] -%}
inline void update_rule_{{-rule["num"]-}}(){
propensity_sum  -= propensity[{{rule["num"]}}];
        propensity[{{rule["num"]}}] = {{rule["rate"]}}
        {%- for elem in rule["in"] -%}
                * populations[{{elem}}]
        {%- endfor -%}
        ;
        {% for min_s in rule['min_of'].keys() %}
        if (populations[{{min_s}}] < {{rule['min_of'][min_s]}}){
            propensity[{{rule["num"]}}] = 0.0;
        }
        {% endfor -%}
        propensity_sum  += propensity[{{rule["num"]}}];
        }
{% endfor -%}


void step(){
        step_counter += 1;
        if (propensity_sum <= 0.0) {
            throw std::runtime_error("No reaction possible!");
        }
        const double timestep = exp(propensity_sum);
        simtime += timestep;
        double max = randomDouble()*propensity_sum;

          size_t x;
          double sum = 0.0;
          for (x=0;sum <= max; x++){
              sum += propensity[x]  ;
          }
          x -= 1;


switch (x) {
{% for r in model["rules"] %}
    case {{r.num}} :  {
            // Update state
        {%- for elem in r["opt_in"] %}
            populations[{{elem}}]-=1;
            {%- endfor -%}
             {%- for elem in r["opt_out"] %}
            populations[{{elem}}]+=1;
        {%- endfor %}

        // Update Propensities
        {% for affr in model["affected_reaction"][r["num"]] -%}
           update_rule_{{-affr-}}();
            {# model.propensity_sum  -= model.propensity[{{model["rules"][affr]["num"]}}];
            model.propensity[{{affr}}] = {{model["rules"][affr]["rate"]}}
            {%- for elem in model["rules"][affr]["in"] -%}
                    * model.populations[{{elem}}] as f64
            {%- endfor -%}
            ;
            {%- for min_s in model["rules"][affr]['min_of'].keys() %}
            if (model.populations[{{min_s}}] < {{model["rules"][affr]['min_of'][min_s]}}){
                model.propensity[{{model["rules"][affr]["num"]}}] = 0.0;
            }
            {% endfor -%}
            model.propensity_sum  += model.propensity[{{affr}}];#}
        {%- endfor -%}
    break;
    }
{%- endfor -%}
default: //panic!("Reaction selection Error!")
    throw std::runtime_error("Reaction Selection error!");
    break;
}
}







void run_until(double target_time){
    while (simtime < target_time){
        step();
    }
}
void model_observe(size_t obs_num){
      {% for e in model["observations"].keys() -%}
            observations[obs_num][{{loop.index0}}] = 0
            {%- for n in model["observations"][e] -%}
                + populations[{{n}}]
            {%- endfor -%}
            ;
        {% endfor %}
}


int main() {

    const double delta_t = {{model["until"]}} * 1.0 / {{model["num_steps"]}};
    {% for rule in model["rules"] -%}
    update_rule_{{-rule["num"]-}}();
    {% endfor -%}
    auto start = std::chrono::high_resolution_clock::now();
    //print_first_line();
    model_observe(0);

    for(int c = 1; c < {{model["num_steps"]}}; c++){
        run_until(c * delta_t);
        model_observe(c);
    }
    run_until({{model["until"]}});
    model_observe({{model["num_steps"]}});
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;

    std::ofstream myfile;
    myfile.open ("cpp_observations.txt");
    myfile << "#time\t{%- for e in model["observations"].keys() -%}{{-e-}}\t{%- endfor -%}";
    for (int line = 0; line <= {{model["num_steps"]}}; line++ ){
        myfile<< delta_t *line<<"\t";
        for (auto& k : observations[line]){
            myfile <<k<<"\t";
        }
        myfile<<"\n";
    }
    myfile.close();

    std::cout<< "# -C- "<<step_counter<<" Steps in "<<elapsed.count() << " s -> "<<step_counter/(elapsed.count() *1e6) <<" steps per mu_sec\n";

    return 0;
}