//use rand::prelude::*;
use std::time::Instant;
//use time::Duration;
//use rand::rngs::SmallRng;
//use rayon::prelude::*;


use std::fs::File;
use std::io::{Write};

//use struct_mod::*;

use rust_ssa_model::ModelStruct;



fn main() {
        let delta_t : f64 = {{model["until"]}} / {{model["num_steps"]}} as f64;
        //let results : Vec<[[usize;{{model["num_steps"]+1}}];{{ model["observations"]|length}}]> = (0..{{"1"}}).into_par_iter().map(|_| {
        //let results : [usize;{{model["num_steps"]+1}}];{{ model["observations"]|length}}];
        let now = Instant::now();
        let mut m = ModelStruct::new();

        //m.print_first_line();
        m.model_observe(0);
        for c in 1..{{model["num_steps"]}}{
            m.run_until(c as f64  * delta_t );
            m.model_observe(c)
        }
        m.run_until({{model["until"]}});
        m.model_observe({{model["num_steps"]}});
        let new_now = Instant::now();
        let dur = new_now.duration_since(now);
        {% if 1 == 1 %}
        println!(
        "# --- {} Steps in {} ->  {} steps per mu_sec",
        m.step_counter,dur.as_secs_f64(),
        m.step_counter as f64 /dur.as_micros() as f64
        );

        {% endif %}


    {% for e in model["observations"].keys() -%}
    //    let mut output = File::create("obs_{{-e-}}.txt").unwrap();
    //    for i in 0..={{model["num_steps"]}}{
    //        write!(output,"{} ",i as f64 * delta_t).unwrap();
    //        //for rep in 0..{{"1"}}{
    //            write!(output,"{} ",m.observations[{{loop.index0}}][i]).unwrap();
    //        //}
    //        write!(output,"\n").unwrap();
    //    }
    {%- endfor %}

    let mut output = File::create("observations_specific.txt").unwrap();
    write!(output, "# time\t").unwrap();
    {% for e in model["observations"].keys() -%}
        write!(output, "{}\t", "{{e}}").unwrap();
    {%- endfor -%}
    for step in 0..{{model["num_steps"]+1}}  {
        write!(output, "\n{}\t", step as f64 * delta_t).unwrap();
        m.observations
            .iter()
            .map(|o| o[step])
            .for_each(|o| write!(output, "{}\t", o).unwrap());
    }

}