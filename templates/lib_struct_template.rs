use rand::prelude::*;
use rand::rngs::SmallRng;

#[derive(Clone)]
pub struct ModelStruct {
    pub propensity: [f64; {{model["rules"]|length }}],
    pub populations: [usize; {{model["init"]|length}}],
    pub propensity_sum: f64,
    pub time: f64,
    //rng: ThreadRng,// Thread Rng is slower but more "Random"

    rng : SmallRng,

    pub step_counter : usize,
    {% if model.count_rules -%}
    reaction_exec_counts:[usize; {{model["rules"]|length}}],
    {%- endif %}


    pub observations : [[usize;{{model["num_steps"]+1}}];{{ model["observations"]|length}}],
}

impl ModelStruct {
    pub fn new() -> ModelStruct {

        let mut m = ModelStruct {
            propensity: [0.0; {{model["rules"]|length}}],
            propensity_sum: 0.0,
            time : 0.0,
            populations: [
            {%- for v in model["init"] -%}
            {{v}},
            {%- endfor -%}
            ],
            //rng: thread_rng(),
            //rng: SmallRng::from_seed(seed),

            rng : SmallRng::from_entropy(),


            observations: [[1234;{{model["num_steps"]+1}}];{{ model["observations"]|length}}],
            step_counter : 0,
            {%- if model.count_rules -%}
            reaction_exec_counts: [0; {{model.num_rules}}],
            {%- endif -%}
        };
        m.init();
        m
    }

pub fn init(&mut self){
{% for rn in model["rules"] %}
    //fn update_rule_{{-rn["num"]}}
        self.propensity[{{rn["num"]}}] = {{rn["rate"]}}
        {% for elem in rn["in"] -%}
                * self.populations[{{elem}}] as f64
        {%- endfor -%}
        ;
        {% for min_s in rn['min_of'].keys() %}
        if self.populations[{{min_s}}] < {{rn['min_of'][min_s]}}{
            self.propensity[{{rn["num"]}}] = 0.0;
        }
        {% endfor %}
{% endfor -%}
    self.propensity_sum = self.propensity.iter().sum();
}

{% for rule in model["rules"] -%}

{% endfor -%}

}


{#{% for r in model["rules"] %}
mod reactions_mod_{{-r['num']}};
use reactions_mod_{{-r['num']}}::*;
{% endfor %}
#}

mod reactions_mod_master;
use reactions_mod_master::*;

//mod struct_mod;
//use struct_mod::*;


impl ModelStruct {

    pub fn get_random_uniform_propensity(&mut self) -> f64 {
        self.rng.gen_range(0.0, self.propensity_sum)
    }
    pub fn get_needed_random_numbers(&mut self) -> (f64, f64) {
        if self.propensity_sum <= 0.0 {
            panic!("No reaction possible!")
        }
        let distr = rand_distr::Exp::new(self.propensity_sum).unwrap();
        let timestep = distr.sample(&mut self.rng);
        (timestep, self.get_random_uniform_propensity())
    }

    pub fn select_next(&mut self, max: f64) -> usize {
        self.propensity
            .iter()
            .scan(0.0, |sum, p| {
                *sum += p;
                Some(*sum)
            })
            .position(|p| p > max)
            .expect("No reaction was selected")
    }

pub fn execute(&mut self, x : usize){
match x {
{% for r in model["rules"] %}
    {{r["num"]}} =>
    execute_rule_{{-r["num"]-}}( self),
{%- endfor -%}
_ => panic!("Reaction selection Error!")
}
}

    pub fn step(&mut self) {
        self.step_counter += 1;
        let (timestep, max) = self.get_needed_random_numbers();
        self.time += timestep;
        let x = self.select_next(max);
        self.execute(x);
    }

pub fn print_first_line(&self){
println!("time {% for e in model['observations'].keys() -%}
    {{e}} {% endfor -%}");
}

pub fn run_until(&mut self,target_time :f64){
while self.time < target_time{
self.step();
}
}
pub fn model_observe(&mut self,obs_num : usize){
        {% for e in model["observations"].keys() -%}
            // {{e}}
            self.observations[{{loop.index0}}][obs_num] = 0
            {%- for n in model["observations"][e] -%}
                + self.populations[{{n}}]
            {%- endfor -%}
            ;

        {% endfor %}
}
}
