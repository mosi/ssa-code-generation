use std::fs::File;
use std::io::Write;
use std::time::Instant;

use rust_ssa_model::ModelStruct;
use std::path::Path;

use clap;

fn main() {
    let matches = clap::App::new("Generic Simulator")
        .arg(
            clap::Arg::with_name("INPUT_FILE")
                .required(true)
                .help("The toml file that contains the model description")
                .index(1),
        )
        .arg(
            clap::Arg::with_name("count")
                .long("count")
                .help("Count how often a reaction happens"),
        )
        .get_matches();

    let inp_name = matches.value_of("INPUT_FILE").unwrap();
    assert!(inp_name.contains(".toml"));
    let inp_path = Path::new(inp_name);

    let count_matches = matches.is_present("count");

    let mut m = ModelStruct::new(inp_path, count_matches);
    let now = Instant::now();
    m.model_observe();
    for c in 1..m.num_steps {
        m.run_until(c as f64 * m.delta_t);
        m.model_observe()
    }
    m.run_until(m.until);
    m.model_observe();
    let new_now = Instant::now();
    let dur = new_now.duration_since(now);

    println!(
        "# --- {} Steps in {} s ->  {} steps per mu_sec",
        m.step_counter,
        dur.as_secs_f64(),
        m.step_counter as f64 / dur.as_micros() as f64
    );

    //let mut output = File::create(inp_name.replace(".toml", "") + "observations.txt").unwrap();
    let mut output = File::create("observations_generic.txt").unwrap();
    write!(output, "# time\t").unwrap();
    for ob_name in m.obs_names.iter() {
        write!(output, "{}\t", ob_name).unwrap();
    }
    for step in 0..m.num_steps + 1 {
        write!(output, "\n{}\t", step as f64 * m.delta_t).unwrap();
        m.observations
            .iter()
            .map(|o| o[step])
            .for_each(|o| write!(output, "{}\t", o).unwrap());
    }

    if count_matches{
        m.print_final_counts(Path::new("reaction_counts.toml"));
    }

}
