use criterion::*;
use rand::Rng;
use rust_ssa_model::ModelStruct;
use std::time::Duration;

const MODEL_WARMUP_STEPS: usize = 10000;
const ITERATION_SIZES: [u64; 6] = [1,4,16, 64, 256,1024];

pub fn only_rand(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    c.bench_function("rand only",|b|b.iter(||black_box(m.get_needed_random_numbers())));
}

pub fn only_select(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    c.bench_function("select only",|b|b.iter(||{
        let (t,x) = m.get_needed_random_numbers();
        black_box(t);
        black_box(m.select_next(x))}));
}

pub fn b_step(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    let mut rng = rand::thread_rng();

    let mut group = c.benchmark_group("steps");
    for num_s in ITERATION_SIZES.iter() {
        group.throughput(Throughput::Elements(*num_s as u64));
        group.bench_with_input(format!("{}", num_s), num_s, |b, num_s| {
            b.iter_batched_ref(
                || {
                    let mut m2 = m.clone();
                    let sampling_start =
                        rng.gen_range(0, ITERATION_SIZES.last().unwrap() - *num_s + 1);
                    /* println!(
                        "Sampling from {} to {}",
                        sampling_start,
                        sampling_start + *num_s
                    );*/
                    for _ in 0..sampling_start {
                        m2.step();
                    }
                    m2
                },
                |m2| {
                    for _ in 0..*num_s {
                        m2.step()
                    }
                },
                BatchSize::SmallInput,
            )
        });
    }

    group.finish();
}

pub fn b_step_rnd(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    let mut rng = rand::thread_rng();

    let mut group = c.benchmark_group("steps + rnd");
    for num_s in ITERATION_SIZES.iter() {
        group.throughput(Throughput::Elements(*num_s as u64));
        group.bench_with_input(format!("{}", num_s), num_s, |b, num_s| {
            b.iter_batched_ref(
                || {
                    let mut m2 = m.clone();
                    let sampling_start =
                        rng.gen_range(0, ITERATION_SIZES.last().unwrap() - *num_s + 1);
                    /* println!(
                        "Sampling from {} to {}",
                        sampling_start,
                        sampling_start + *num_s
                    );*/
                    for _ in 0..sampling_start {
                        m2.step();
                    }
                    m2
                },
                |m2| {
                    for _ in 0..*num_s {
                        m2.step();
                        black_box(m2.get_needed_random_numbers());
                    }
                },
                BatchSize::SmallInput,
            )
        });
    }

    group.finish();
}

pub fn b_step_replay(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    let mut rng = rand::thread_rng();

    let mut group = c.benchmark_group("execute replay");
    for num_s in ITERATION_SIZES.iter() {
        group.throughput(Throughput::Elements(*num_s as u64));
        group.bench_with_input(format!("{}", num_s), num_s, |b, num_s| {
            b.iter_batched_ref(
                || {
                    let mut m2 = m.clone();
                    let sampling_start =
                        rng.gen_range(0, ITERATION_SIZES.last().unwrap() - *num_s + 1);
                    /* println!(
                        "Sampling from {} to {}",
                        sampling_start,
                        sampling_start + *num_s
                    );*/
                    let mut steplist: Vec<usize> = vec![];
                    for _ in 0..sampling_start {
                        m2.step()
                    }
                    let mut run_clone = m2.clone();
                    for _ in 0..*num_s {
                        run_clone.step_counter += 1;
                        let (timestep, max) = run_clone.get_needed_random_numbers();
                        black_box(timestep);
                        let x = run_clone.select_next(max);
                        steplist.push(x);
                        run_clone.execute(x);
                    }
                    (m2, steplist)
                },
                |(m2, list)| {
                    for x in list {
                        m2.step_counter += 1;
                        //let x = m2.select_next();
                        m2.execute(black_box(*x));
                    }
                },
                BatchSize::SmallInput,
            )
        });
    }

    group.finish();
}

pub fn b_step_replay_select(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    let mut rng = rand::thread_rng();

    let mut group = c.benchmark_group("select+execute replay2");
    for num_s in ITERATION_SIZES.iter() {
        group.throughput(Throughput::Elements(*num_s as u64));
        group.bench_with_input(format!("{}", num_s), num_s, |b, num_s| {
            b.iter_batched_ref(
                || {
                    let mut m2 = m.clone();
                    let sampling_start =
                        rng.gen_range(0, ITERATION_SIZES.last().unwrap() - *num_s + 1);
                    /* println!(
                        "Sampling from {} to {}",
                        sampling_start,
                        sampling_start + *num_s
                    );*/
                    let mut steplist: Vec<usize> = vec![];
                    for _ in 0..sampling_start {
                        m2.step()
                    }
                    let mut run_clone = m2.clone();
                    for _ in 0..*num_s {
                        run_clone.step_counter += 1;
                        let (timestep, max) = run_clone.get_needed_random_numbers();
                        run_clone.time += timestep;
                        let x = run_clone.select_next(max);
                        steplist.push(x);
                        run_clone.execute(x);
                    }
                    (m2, steplist)
                },
                |(m2, list)| {
                    for x in list {
                        m2.step_counter += 1;
                        let (timestep, max) = m2.get_needed_random_numbers();
                        m2.time += timestep;
                        black_box(m2.select_next(max));
                        m2.execute(black_box(*x));
                    }
                },
                BatchSize::SmallInput,
            )
        });
    }

    group.finish();
}

pub fn b_step_select(c: &mut Criterion) {
    let mut m = ModelStruct::new();
    for _ in 0..MODEL_WARMUP_STEPS {
        m.step();
    }
    let mut rng = rand::thread_rng();

    let mut group = c.benchmark_group("steps + select");
    for num_s in ITERATION_SIZES.iter() {
        group.throughput(Throughput::Elements(*num_s as u64));
        group.bench_with_input(format!("{}", num_s), num_s, |b, num_s| {
            b.iter_batched_ref(
                || {
                    let mut m2 = m.clone();
                    let sampling_start =
                        rng.gen_range(0, ITERATION_SIZES.last().unwrap() - *num_s + 1);
                    /* println!(
                        "Sampling from {} to {}",
                        sampling_start,
                        sampling_start + *num_s
                    );*/
                    for _ in 0..sampling_start {
                        m2.step();
                    }
                    m2
                },
                |m2| {
                    for _ in 0..*num_s {
                        m2.step();
                        let (timestep, max) = m2.get_needed_random_numbers();
                        black_box(timestep);
                        black_box(m2.select_next(max));
                    }
                },
                BatchSize::SmallInput,
            )
        });
    }
    group.finish();
}

criterion_group!(name = impr_benches;
                 config= Criterion::default();//.warm_up_time(Duration::from_millis(150)).measurement_time(Duration::from_millis(1500)) ;/*.nresamples(100);*/
                targets =b_step,b_step_rnd,b_step_select,b_step_replay,b_step_replay_select);

criterion_group!(name = regl_benches;
                 config= Criterion::default();//.warm_up_time(Duration::from_millis(150)).measurement_time(Duration::from_millis(1500)) ;/*.nresamples(100);*/
                targets = only_rand,only_select);
criterion_main!(impr_benches,regl_benches);